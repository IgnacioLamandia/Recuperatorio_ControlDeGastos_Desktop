import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.uqbar.arena.bindings.ObservableValue;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Control;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.NumericField;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.TextBox;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.SimpleWindow;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.arena.xtend.ArenaXtendExtensions;
import org.uqbar.lacar.ui.model.Action;
import org.uqbar.lacar.ui.model.ControlBuilder;
import org.uqbar.lacar.ui.model.TableBuilder;
import org.uqbar.lacar.ui.model.bindings.ViewObservable;

@SuppressWarnings("all")
public class ControlDeGastosWindow extends SimpleWindow<ControlDeGastosAppModel> {
  public ControlDeGastosWindow(final WindowOwner parent, final ControlDeGastosAppModel model) {
    super(parent, model);
    this.setTitle("Control de gastos");
    ControlDeGastosAppModel _modelObject = this.getModelObject();
    _modelObject.inicializar();
  }
  
  public void createMainTemplate(final Panel mainPanel) {
    ColumnLayout _columnLayout = new ColumnLayout(1);
    mainPanel.setLayout(_columnLayout);
    Panel panelBienvenida = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout = new HorizontalLayout();
    panelBienvenida.setLayout(_horizontalLayout);
    this.crearLabel(panelBienvenida, "Hola ", "usuarioLogeado.nombre", " estos son tus gastos");
    Panel panelGastoTotal = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout_1 = new HorizontalLayout();
    panelGastoTotal.setLayout(_horizontalLayout_1);
    this.crearLabel(panelGastoTotal, "Llevas gastado: $", "usuarioLogeado.gastoTotal", "en total");
    Panel panelDeFiltrado = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout_2 = new HorizontalLayout();
    panelDeFiltrado.setLayout(_horizontalLayout_2);
    Label _label = new Label(panelDeFiltrado);
    final Procedure1<Label> _function = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Filtrar por Descripcion:");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label, _function);
    TextBox _textBox = new TextBox(panelDeFiltrado);
    final Procedure1<TextBox> _function_1 = new Procedure1<TextBox>() {
      public void apply(final TextBox it) {
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "filtrador.descripcion");
      }
    };
    ObjectExtensions.<TextBox>operator_doubleArrow(_textBox, _function_1);
    Button _button = new Button(panelDeFiltrado);
    Button _setCaption = _button.setCaption("Filtrar");
    final Action _function_2 = new Action() {
      public void execute() {
        ControlDeGastosAppModel _modelObject = ControlDeGastosWindow.this.getModelObject();
        _modelObject.filtrar();
      }
    };
    Button _onClick = _setCaption.onClick(_function_2);
    _onClick.setAsDefault();
    Label _label_1 = new Label(panelDeFiltrado);
    final Procedure1<Label> _function_3 = new Procedure1<Label>() {
      public void apply(final Label it) {
        it.setText("Gastos de las descripcion buscada = $");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_1, _function_3);
    Label _label_2 = new Label(panelDeFiltrado);
    final Procedure1<Label> _function_4 = new Procedure1<Label>() {
      public void apply(final Label it) {
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "sumarGastosFiltrados");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_2, _function_4);
    Panel panel = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout_3 = new HorizontalLayout();
    panel.setLayout(_horizontalLayout_3);
    Panel panelTabla = new Panel(panel);
    Table<Gasto> _table = new Table<Gasto>(panelTabla, Gasto.class);
    final Procedure1<Table<Gasto>> _function_5 = new Procedure1<Table<Gasto>>() {
      public void apply(final Table<Gasto> it) {
        it.setNumberVisibleRows(9);
        ViewObservable<Table<Gasto>, TableBuilder<Gasto>> _items = it.items();
        ArenaXtendExtensions.operator_spaceship(_items, "gastos");
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "gastoSeleccionado");
        Column<Gasto> _column = new Column<Gasto>(it);
        final Procedure1<Column<Gasto>> _function = new Procedure1<Column<Gasto>>() {
          public void apply(final Column<Gasto> it) {
            it.setTitle("Mes");
            it.setFixedSize(100);
            it.bindContentsToProperty("fechaRegistro.month");
          }
        };
        ObjectExtensions.<Column<Gasto>>operator_doubleArrow(_column, _function);
        Column<Gasto> _column_1 = new Column<Gasto>(it);
        final Procedure1<Column<Gasto>> _function_1 = new Procedure1<Column<Gasto>>() {
          public void apply(final Column<Gasto> it) {
            it.setTitle("Descripcion");
            it.setFixedSize(200);
            it.bindContentsToProperty("descripcion");
          }
        };
        ObjectExtensions.<Column<Gasto>>operator_doubleArrow(_column_1, _function_1);
        Column<Gasto> _column_2 = new Column<Gasto>(it);
        final Procedure1<Column<Gasto>> _function_2 = new Procedure1<Column<Gasto>>() {
          public void apply(final Column<Gasto> it) {
            it.setTitle("Monto");
            it.setFixedSize(100);
            it.bindContentsToProperty("monto");
          }
        };
        ObjectExtensions.<Column<Gasto>>operator_doubleArrow(_column_2, _function_2);
      }
    };
    ObjectExtensions.<Table<Gasto>>operator_doubleArrow(_table, _function_5);
    Panel panelIndice = new Panel(mainPanel);
    HorizontalLayout _horizontalLayout_4 = new HorizontalLayout();
    panel.setLayout(_horizontalLayout_4);
    Button _button_1 = new Button(panelIndice);
    Button _setCaption_1 = _button_1.setCaption("Consultar Indice Inflacionario");
    final Action _function_6 = new Action() {
      public void execute() {
        ControlDeGastosAppModel _modelObject = ControlDeGastosWindow.this.getModelObject();
        _modelObject.calcularIndice();
      }
    };
    Button _onClick_1 = _setCaption_1.onClick(_function_6);
    _onClick_1.setAsDefault();
    Label _label_3 = new Label(panelIndice);
    final Procedure1<Label> _function_7 = new Procedure1<Label>() {
      public void apply(final Label it) {
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "indice");
      }
    };
    ObjectExtensions.<Label>operator_doubleArrow(_label_3, _function_7);
    Panel panelNuevoGasto = new Panel(panel);
    ColumnLayout _columnLayout_1 = new ColumnLayout(1);
    panelNuevoGasto.setLayout(_columnLayout_1);
    Panel panelGasto = new Panel(panelNuevoGasto);
    VerticalLayout _verticalLayout = new VerticalLayout();
    panelGasto.setLayout(_verticalLayout);
    Label _label_4 = new Label(panelGasto);
    _label_4.setText("Nuevo Gasto");
    Label _label_5 = new Label(panelGasto);
    _label_5.setText("Descripcion");
    TextBox _textBox_1 = new TextBox(panelGasto);
    final Procedure1<TextBox> _function_8 = new Procedure1<TextBox>() {
      public void apply(final TextBox it) {
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "gasto.descripcion");
      }
    };
    ObjectExtensions.<TextBox>operator_doubleArrow(_textBox_1, _function_8);
    Label _label_6 = new Label(panelGasto);
    _label_6.setText("Monto");
    NumericField _numericField = new NumericField(panelGasto);
    final Procedure1<NumericField> _function_9 = new Procedure1<NumericField>() {
      public void apply(final NumericField it) {
        ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
        ArenaXtendExtensions.operator_spaceship(_value, "gasto.monto");
      }
    };
    ObjectExtensions.<NumericField>operator_doubleArrow(_numericField, _function_9);
    Button _button_2 = new Button(panelNuevoGasto);
    Button _setCaption_2 = _button_2.setCaption("Agregar");
    final Action _function_10 = new Action() {
      public void execute() {
        ControlDeGastosAppModel _modelObject = ControlDeGastosWindow.this.getModelObject();
        _modelObject.agregar();
      }
    };
    Button _onClick_2 = _setCaption_2.onClick(_function_10);
    _onClick_2.setAsDefault();
  }
  
  public Label crearLabel(final Panel panel, final String label1, final String binding, final String label2) {
    Label _xblockexpression = null;
    {
      Label _label = new Label(panel);
      _label.setText(label1);
      Label _label_1 = new Label(panel);
      final Procedure1<Label> _function = new Procedure1<Label>() {
        public void apply(final Label it) {
          ObservableValue<Control, ControlBuilder> _value = it.<ControlBuilder>value();
          ArenaXtendExtensions.operator_spaceship(_value, binding);
        }
      };
      ObjectExtensions.<Label>operator_doubleArrow(_label_1, _function);
      Label _label_2 = new Label(panel);
      _xblockexpression = _label_2.setText(label2);
    }
    return _xblockexpression;
  }
  
  protected void addActions(final Panel actionsPanel) {
    throw new UnsupportedOperationException("TODO: auto-generated method stub");
  }
  
  protected void createFormPanel(final Panel mainPanel) {
    throw new UnsupportedOperationException("TODO: auto-generated method stub");
  }
}
