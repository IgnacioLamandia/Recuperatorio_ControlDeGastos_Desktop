import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.CollectionExtensions;
import org.uqbar.arena.Application;
import org.uqbar.arena.windows.Window;

@Accessors
@SuppressWarnings("all")
public class ControlDeGastosApplication extends Application {
  public static void main(final String[] args) {
    ControlDeGastosApplication _controlDeGastosApplication = new ControlDeGastosApplication();
    _controlDeGastosApplication.start();
  }
  
  protected Window<?> createMainWindow() {
    final List<Usuario> usuarios = new ArrayList<Usuario>();
    final Usuario tefi = new Usuario("Tefi");
    final Usuario tincho = new Usuario("Tincho");
    final Usuario gise = new Usuario("Gise");
    final Usuario denna = new Usuario("Denna");
    final Gasto gasto1 = new Gasto("Heladeria", 100);
    final Gasto gasto2 = new Gasto("Super Mercado", 200);
    final Gasto gasto3 = new Gasto("Cine", 300);
    final Gasto gasto4 = new Gasto("Heladeria", 400);
    final Gasto gasto5 = new Gasto("Cine", 500);
    final Gasto gasto6 = new Gasto("Super Mercado", 700);
    final Gasto gasto7 = new Gasto("Cine", 800);
    final Gasto gasto8 = new Gasto("Heladeria", 900);
    final Gasto gasto9 = new Gasto("Cine", 1000);
    tefi.nuevoGasto(gasto1);
    tefi.nuevoGasto(gasto2);
    tefi.nuevoGasto(gasto3);
    tefi.nuevoGasto(gasto4);
    tincho.nuevoGasto(gasto5);
    tincho.nuevoGasto(gasto6);
    tincho.nuevoGasto(gasto7);
    gise.nuevoGasto(gasto8);
    gise.nuevoGasto(gasto9);
    CollectionExtensions.<Usuario>addAll(usuarios, denna, tincho, gise, tefi);
    final RepoUsuarios repo = new RepoUsuarios(usuarios);
    final ControlDeGastosAppModel modulo = new ControlDeGastosAppModel(repo);
    modulo.logearUsuario(tefi);
    return new ControlDeGastosWindow(this, modulo);
  }
}
