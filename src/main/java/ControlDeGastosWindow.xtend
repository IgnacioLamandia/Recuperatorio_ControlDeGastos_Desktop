import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.NumericField
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*

class ControlDeGastosWindow extends SimpleWindow<ControlDeGastosAppModel>{
	new(WindowOwner parent, ControlDeGastosAppModel model) {
		super(parent, model)
		title = "Control de gastos"
		modelObject.inicializar
	}
	
	override def createMainTemplate(Panel mainPanel) {
		mainPanel.layout = new ColumnLayout(1)
		
		//Panel de bienvenida
		var panelBienvenida = new Panel(mainPanel)
		
			panelBienvenida.setLayout(new HorizontalLayout)
				
		crearLabel(panelBienvenida, "Hola ", "usuarioLogeado.nombre", " estos son tus gastos")
		
		var panelGastoTotal = new Panel(mainPanel)
		
			panelGastoTotal.setLayout(new HorizontalLayout)
		crearLabel(panelGastoTotal, "Llevas gastado: $", "usuarioLogeado.gastoTotal", "en total")

		
		//Panel de filtrado
		var panelDeFiltrado = new Panel(mainPanel)
		
			panelDeFiltrado.setLayout(new HorizontalLayout)				
			new Label(panelDeFiltrado) =>[
				 text = "Filtrar por Descripcion:" 			
			]	
		
			new TextBox(panelDeFiltrado) => [
				value <=> "filtrador.descripcion"
			]
		
			new Button(panelDeFiltrado)
				.setCaption("Filtrar")
				.onClick [ | modelObject.filtrar ] 
				.setAsDefault
				
			new Label(panelDeFiltrado) =>[
				text = "Gastos de las descripcion buscada = $"
			]
			new Label(panelDeFiltrado) => [
			value <=> "sumarGastosFiltrados"
		]	
		
		//Tabla de gastos
		var panel = new Panel(mainPanel)
		panel.setLayout(new HorizontalLayout)
		
			var panelTabla = new Panel(panel)	
			//Creamos la tabla para mostrar los datos de los gastos	
			new Table<Gasto>(panelTabla, typeof(Gasto)) => [
			numberVisibleRows = 9
			//bindeamos los datos
			items <=> "gastos"
			value <=> "gastoSeleccionado"
			
			//Definimos las columnas	
				new Column<Gasto>(it) => [
					title = "Mes"
					fixedSize = 100
					bindContentsToProperty("fechaRegistro.month")
				]
	
				new Column<Gasto>(it) => [
					title = "Descripcion"
					fixedSize = 200
					bindContentsToProperty("descripcion")
				]	
				new Column<Gasto>(it) => [
					title = "Monto"
					fixedSize = 100
					bindContentsToProperty("monto")
				]
		
			]
			
			//Boton indice inflacionario		
 			var panelIndice = new Panel(mainPanel)
			panel.setLayout(new HorizontalLayout)
			
			new Button(panelIndice)
				.setCaption("Consultar Indice Inflacionario")
				.onClick [ | modelObject.calcularIndice ] 
				.setAsDefault
				
			new Label(panelIndice) => [
			value <=> "indice"
			]
					
			//Panel de nuevo gasto
			var panelNuevoGasto = new Panel(panel)
				panelNuevoGasto.layout = new ColumnLayout(1)
				
				var panelGasto = new Panel(panelNuevoGasto)
					panelGasto.setLayout(new VerticalLayout)
					new Label(panelGasto).text = "Nuevo Gasto"
					
				
					new Label(panelGasto).text = "Descripcion"
					
					new TextBox(panelGasto) => [
					value <=> "gasto.descripcion"
				]
				
					new Label(panelGasto).text = "Monto"
					
					new NumericField(panelGasto) => [
					value <=> "gasto.monto"
				]
					

				new Button(panelNuevoGasto)
				.setCaption("Agregar")
				.onClick [ | modelObject.agregar ] 
				.setAsDefault	

	}
	
	def crearLabel(Panel panel, String label1, String binding, String label2) {
		
		new Label(panel).text = label1
		
		new Label(panel) => [
			value <=> binding
		]
		new Label(panel).text = label2
		
	}
	
	//var gastoSeleccionado = new NotNullObservable("gastoSeleccionado")
	
	override protected addActions(Panel actionsPanel) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
	override protected createFormPanel(Panel mainPanel) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
	
}