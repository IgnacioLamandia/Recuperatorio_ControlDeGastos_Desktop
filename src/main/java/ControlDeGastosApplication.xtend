import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import java.util.List
import java.util.ArrayList

@Accessors
class ControlDeGastosApplication extends Application {
	
	static def void main(String[] args) { 
		new ControlDeGastosApplication().start()
	}

	override protected Window<?> createMainWindow() {
		
		//Usuarios
		val List<Usuario> usuarios = new ArrayList<Usuario>()
		
		val tefi = new Usuario("Tefi")
		val tincho = new Usuario("Tincho")
		val gise = new Usuario("Gise")
		val denna = new Usuario("Denna")

		//Gastos
		val gasto1 = new Gasto("Heladeria", 100)
		val gasto2 = new Gasto("Super Mercado", 200)
		val gasto3 = new Gasto("Cine", 300)
		val gasto4 = new Gasto("Heladeria", 400)
		val gasto5 = new Gasto("Cine", 500)		
		val gasto6 = new Gasto("Super Mercado", 700)
		val gasto7 = new Gasto("Cine", 800)
		val gasto8 = new Gasto("Heladeria", 900)
		val gasto9 = new Gasto("Cine", 1000)
		
		tefi.nuevoGasto(gasto1)
		tefi.nuevoGasto(gasto2)
		tefi.nuevoGasto(gasto3)
		tefi.nuevoGasto(gasto4)
		tincho.nuevoGasto(gasto5)
		tincho.nuevoGasto(gasto6)
		tincho.nuevoGasto(gasto7)
		gise.nuevoGasto(gasto8)
		gise.nuevoGasto(gasto9)

		usuarios.addAll(denna, tincho, gise, tefi)
		val RepoUsuarios repo = new RepoUsuarios(usuarios)	
		
		val ControlDeGastosAppModel modulo = new ControlDeGastosAppModel(repo)
		modulo.logearUsuario(tefi)
		
		return new ControlDeGastosWindow(this, modulo)
	}
}